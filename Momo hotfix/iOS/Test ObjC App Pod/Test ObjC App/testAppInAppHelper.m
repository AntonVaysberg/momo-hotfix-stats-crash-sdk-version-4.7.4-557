//
//  testAppInAppHelper.m
//  testApp
//
//  Created by Golan's Mac on 12/31/14.
//  Copyright (c) 2014 AppsFlyer. All rights reserved.
//

#import "testAppInAppHelper.h"
#import "AppDelegate.h"

@implementation testAppInAppHelper


+(testAppInAppHelper *) sharedInstance {
    static dispatch_once_t once;
    static testAppInAppHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      kConsumableItem,
                                      kNonConsumableItem,
                                      kAutoRenewableSubscriptionItem,
                                      kNonRenewableSubscriptionItem,
                                      nil];
        
        
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;

}

@end
