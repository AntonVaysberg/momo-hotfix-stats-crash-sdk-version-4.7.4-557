//
//  ViewController.m
//  testApp
//
//  Created by Golan's Mac on 11/24/14.
//  Copyright (c) 2014 AppsFlyer. All rights reserved.
//

#import "ViewController.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>
#import <StoreKit/StoreKit.h>
#import "testAppInAppHelper.h"
#import <iAd/iAd.h>
#import "AppDelegate.h"

@interface ViewController () <ADBannerViewDelegate> {
    NSArray *_products;
    BOOL _bannerIsVisible;
    ADBannerView *_adBanner;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    //[[AppsFlyerTracker sharedTracker] trackEvent:@"pageLoad_1" withValue:@""];    
    
    [self reload];
}

-(void) viewWillAppear:(BOOL)animated {
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:)
                                                   name:IAPHelperProductPurchasedNotification object:nil];
    
    _adBanner = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, self.view.frame.size.width, 50)];
    _adBanner.delegate = self;
    [self.view addSubview:_adBanner];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)onNewPage:(id)sender {
    
}

-(void) callTrackAppLaunch {
 
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
}

-(IBAction)trackEvent1:(id)sender {
    
    [[AppsFlyerTracker sharedTracker] trackEvent:AFEventPurchase
                                      withValues:@{
                                                   AFEventParamContentId : @"123",
                                                   AFEventParamContentType : @"Virtual Goods",
                                                   AFEventParamCurrency : @"GBP",
                                                   AFEventParamRevenue : @"9.99"}
     ];
    

    
//    [[AppsFlyerTracker sharedTracker] monitorSDKDataCall];
}

-(IBAction)trackEvent2:(id)sender {
    [[AppsFlyerTracker sharedTracker] trackEvent:AFEventPurchase
                                      withValues:@{
                                                   AFEventParamContentId : @"456",
                                                   AFEventParamContentType : @"Swit Goods",
                                                   AFEventParamCurrency : @"USD",
                                                   AFEventParamRevenue : @"3.99"}
     ];
}

-(void)validateInappItem:(NSString *) item{
    [_products enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(SKProduct * skProduct, NSUInteger idx, BOOL *stop) {
        if ([skProduct.productIdentifier isEqualToString:item] ) {
            if (skProduct != nil) {
                NSLog(@"Buying InAppConsumable: %@...", skProduct.productIdentifier);
                [[testAppInAppHelper sharedInstance] buyProduct:skProduct];
            }        }
        else {
            //NSLog(@"We didn't find active Item named %@: ", skProduct.productIdentifier);
            
        }
    }];
}

-(IBAction)clickInAppConsumable:(id)sender {
    [self validateInappItem:kConsumableItem];
}

-(IBAction)clickInAppNonConsumable:(id)sender {
    [self validateInappItem:kNonConsumableItem];
}


-(IBAction)clickInAppAutoRenewSubscription:(id)sender {
    [self validateInappItem:kAutoRenewableSubscriptionItem];
}

-(IBAction)clickInAppNonRenewSubscription:(id)sender {
    [self validateInappItem:kNonRenewableSubscriptionItem];
}




- (void) reload {
    _products = nil;
    [[testAppInAppHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            _products = products;
        }
    }];
}


// sandbox user : test@appsflyer.com
// password: Appsflyer1234

- (void)productPurchased:(NSNotification *)notification {
    SKPaymentTransaction *trans = notification.object;
    NSString *productIdentifier = trans.payment.productIdentifier;
    
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            NSLog(@"Purchased: %@", productIdentifier);
            
            
            NSLocale* storeLocale = product.priceLocale;
            NSString *storeCountry = (NSString*)CFLocaleGetValue((CFLocaleRef)storeLocale, kCFLocaleCountryCode);
            
            NSString* currency = nil;
            
            if([storeCountry  isEqual: @"US"]){
                currency = @"USD";
            }
            else if([storeCountry  isEqual: @"IL"]){
                currency = @"NIS";
            }

            
            [[AppsFlyerTracker sharedTracker] validateAndTrackInAppPurchase:product.productIdentifier
                                                                      price:product.price.stringValue
                                                                   currency:currency
                                                              transactionId:trans.transactionIdentifier
                                                       additionalParameters:nil
                                                                    success:^(NSDictionary *result){
                                                                        NSLog(@"Purcahse succeeded And verified!!! response: %@", result.description);
                                                                    }
                                                                    failure:^(NSError *error, id response) {
                                                                        NSLog(@"response = %@", response);
                                                                    }];
            *stop = YES;
            
        }
      }];
   }



- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if (!_bannerIsVisible)
    {
        // If banner isn't part of view hierarchy, add it
        if (_adBanner.superview == nil)
        {
            [self.view addSubview:_adBanner];
        }
        
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
        
        // Assumes the banner view is just off the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = YES;
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"Failed to retrieve ad");
    
    if (_bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        
        // Assumes the banner view is placed at the bottom of the screen.
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = NO;
    }
}



@end
