

#import <UIKit/UIKit.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>

extern  NSString * const kConsumableItem;
extern  NSString * const kNonConsumableItem;
extern  NSString * const kAutoRenewableSubscriptionItem;
extern  NSString * const kNonRenewableSubscriptionItem;



@interface AppDelegate : UIResponder <UIApplicationDelegate, AppsFlyerTrackerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, copy) void(^backgroundTransferCompletionHandler)();
@property (nonatomic, strong) NSURLSession *session;

-(void)fetchNewDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;


@end

