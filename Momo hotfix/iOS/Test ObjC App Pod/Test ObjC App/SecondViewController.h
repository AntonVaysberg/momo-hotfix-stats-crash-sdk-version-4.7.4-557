//
//  SecondViewController.h
//  testApp
//
//  Created by Golan's Mac on 11/24/14.
//  Copyright (c) 2014 AppsFlyer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController


-(IBAction)trackEvent3:(id)sender;
-(IBAction)back:(id)sender;

@end
