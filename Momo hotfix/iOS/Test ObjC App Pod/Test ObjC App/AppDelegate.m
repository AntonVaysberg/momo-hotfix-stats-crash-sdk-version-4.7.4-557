
#import "AppDelegate.h"


  NSString * const kConsumableItem = @"com.appsflyer.testapp.objc.first";
  NSString * const kNonConsumableItem = @"com.appsflyer.testapp.objc.no.cons";
  NSString * const kAutoRenewableSubscriptionItem = @"com.appsflyer.testapp.objc.auto.renew";
  NSString * const kNonRenewableSubscriptionItem = @"com.appsflyer.testapp.objc.non.renew";



@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
        
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = @"WdpTVAcYwmxsaQ4WeTspmh";
    [AppsFlyerTracker sharedTracker].appleAppID = @"1203685537";


    [AppsFlyerTracker sharedTracker].delegate = self;
    [AppsFlyerTracker sharedTracker].isDebug = YES;
    [AppsFlyerTracker sharedTracker].useReceiptValidationSandbox = YES;
    //[AppsFlyerTracker sharedTracker].deviceTrackingDisabled = YES;
    
    [[AppsFlyerTracker sharedTracker] setUserEmails:@[@"testerapp4@appsflyer.com", @"testerapp1@appsflyer.com"] withCryptType:EmailCryptTypeSHA256];

    // Register for Push Notitications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    application.applicationIconBadgeNumber = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(handleEnteredBackground)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    
    return YES;
}

- (void) handleEnteredBackground
{
    [[AppsFlyerTracker sharedTracker] trackEvent:@"on_bg" withValues:@{@"data" : @"123"}];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    [[AppsFlyerTracker sharedTracker] registerUninstall:deviceToken];
}


-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[AppsFlyerTracker sharedTracker] handlePushNotification:userInfo];
}


-(BOOL) application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    [[AppsFlyerTracker sharedTracker] handleOpenUrl:url options:options];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) application:(UIApplication *)application didUpdateUserActivity:(NSUserActivity *)userActivity {
    [[AppsFlyerTracker sharedTracker] didUpdateUserActivity:userActivity];
}

-(BOOL) application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
    
    [[AppsFlyerTracker sharedTracker] continueUserActivity:userActivity restorationHandler:restorationHandler];
    return YES;
}


-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    NSDate *fetchStart = [NSDate date];
    
    [self fetchNewDataWithCompletionHandler:^(UIBackgroundFetchResult result) {
        completionHandler(result);
        
        NSDate *fetchEnd = [NSDate date];
        NSTimeInterval timeElapsed = [fetchEnd timeIntervalSinceDate:fetchStart];
        NSLog(@"Background Fetch Duration: %f seconds", timeElapsed);
        
    }];
}


-(void)fetchNewDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
  
    NSLog(@"fetchNewDataWithCompletionHandler called.");
    
    completionHandler(UIBackgroundFetchResultNoData);

}

-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler{
    NSLog(@"handleEventsForBackgroundURLSession called.");

    self.backgroundTransferCompletionHandler = completionHandler;
    
}


-(void)onConversionDataReceived:(NSDictionary*) installData {
    
    id status = [installData objectForKey:@"af_status"];
    
    if([status isEqualToString:@"Non-organic"]) {
        
        id sourceID = [installData objectForKey:@"media_source"];
        
        id campaign = [installData objectForKey:@"campaign"];
        
        NSLog(@"This is a none organic install. Media source: %@  Campaign: %@",sourceID,campaign);
        
    } else if([status isEqualToString:@"Organic"]) {
        
        NSLog(@"This is an organic install.");
        
    }
    
}

-(void)onConversionDataRequestFailure:(NSError *) error {
    
    NSLog(@"%@",error);
}


- (void) onAppOpenAttribution:(NSDictionary*) attributionData {
    
    NSLog(@"attribution data: %@", attributionData);
}

- (void) onAppOpenAttributionFailure:(NSError *)error {
    NSLog(@"%@",error);

}





@end
