//
//  SecondViewController.m
//  testApp
//
//  Created by Golan's Mac on 11/24/14.
//  Copyright (c) 2014 AppsFlyer. All rights reserved.
//

#import "SecondViewController.h"
#import <AppsFlyerLib/AppsFlyerTracker.h>

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(IBAction)trackEvent3:(id)sender {
    [[AppsFlyerTracker sharedTracker] trackEvent:AFEventPurchase
                                      withValues:@{
                                                   AFEventParamContentId : @"789",
                                                   AFEventParamContentType : @"Sh!t only",
                                                   AFEventParamCurrency : @"USD",
                                                   AFEventParamRevenue : @"0.99"}
     ];

 
}


-(IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    

}


@end
