//
//  ViewController.h
//  testApp
//
//  Created by Golan's Mac on 11/24/14.
//  Copyright (c) 2014 AppsFlyer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


-(IBAction)onNewPage:(id)sender;
-(IBAction)trackEvent1:(id)sender;
-(IBAction)trackEvent2:(id)sender;
-(IBAction)clickInAppConsumable:(id)sender;
-(IBAction)clickInAppNonConsumable:(id)sender;
-(IBAction)clickInAppAutoRenewSubscription:(id)sender;
-(IBAction)clickInAppNonRenewSubscription:(id)sender;




@end

