//
//  testAppInAppHelper.h
//  testApp
//
//  Created by Golan's Mac on 12/31/14.
//  Copyright (c) 2014 AppsFlyer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IAPHelper.h"

@interface testAppInAppHelper : IAPHelper

+(testAppInAppHelper *) sharedInstance;

@end
