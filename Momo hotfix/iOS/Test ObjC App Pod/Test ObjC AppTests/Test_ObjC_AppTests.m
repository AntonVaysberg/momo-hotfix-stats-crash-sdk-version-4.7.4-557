//
//  Test_ObjC_AppTests.m
//  Test ObjC AppTests
//
//  Created by Maxim Shoustin on 3/4/17.
//  Copyright © 2017 Maxim Shoustin. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Test_ObjC_AppTests : XCTestCase

@end

@implementation Test_ObjC_AppTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    XCTAssertEqual(1 + 1, 3, "one plus one should equal two but fail on 3");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
