//
//  AppDelegate.h
//  Test ObjC App StaticFW
//
//  Created by Maxim Shoustin on 2/16/17.
//  Copyright © 2017 Maxim Shoustin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AppsFlyerLib/AppsFlyerTracker.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, AppsFlyerTrackerDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

