//
//  AppDelegate.swift
//  Test Swift App Pod
//
//  Created by Maxim Shoustin on 2/15/17.
//  Copyright © 2017 Maxim Shoustin. All rights reserved.
//

import UIKit
import AppsFlyerLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        AppsFlyerTracker.shared().appsFlyerDevKey = "WdpTVAcYwmxsaQ4WeTspmh"
        AppsFlyerTracker.shared().appleAppID = "1201211634"
        AppsFlyerTracker.shared().isDebug = true
        AppsFlyerTracker.shared().delegate = self
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppsFlyerTracker.shared().trackAppLaunch()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
     func application(_ application: UIApplication,
                              open url: URL,
                              sourceApplication: String?,
                              annotation: Any) -> Bool{
        
        
        return true
    }


}

// MARK: - AppsFlyerTrackerDelegate
extension AppDelegate: AppsFlyerTrackerDelegate {
    
    
    func helper(module:String!, message:String!) -> String!{
        
        var sb:String = ""
       sb.append(" " + module + "=")
        var pad:Int = 17 - module.characters.count
        
        while  pad > 0 {
            pad-=1
            sb.append( " " )
        }
        sb.append(" " + message)
        
        return sb
    }
    
    func onConversionDataReceived(_ installData: [AnyHashable : Any]!) {
        
        print("onConversionDataReceived is called")
        
        if let af_click_lookback = installData["af_click_lookback"] as? String{
            print(helper(module: "af_click_lookback", message: af_click_lookback))
        }
        
        if let af_message = installData["af_message"] as? String{
            print(helper(module: "af_message", message: af_message))
        }
        
        if let media_source = installData["media_source"] as? String{
            print(helper(module: "media_source", message: media_source))
        }
        
        if let cost_cents_USD = installData["cost_cents_USD"] as? String{
            print(helper(module: "cost_cents_USD", message: cost_cents_USD))
        }
        
        if let campaign = installData["campaign"] as? String{
            print(helper(module: "campaign", message: campaign))
        }
        
        if let af_status = installData["af_status"] as? String{
            print(helper(module: "af_status", message: af_status))
        }
        
        if let click_time = installData["click_time"] as? String{
            print(helper(module: "click_time", message: click_time))
        }
        
        if let orig_cost = installData["orig_cost"] as? String{
            print(helper(module: "orig_cost", message: orig_cost))
        }
        
        if let af_dp = installData["af_dp"] as? String{
            print(helper(module: "af_dp", message: af_dp))
        }
        
        if let af_web_dp = installData["af_web_dp"] as? String{
            print(helper(module: "af_web_dp", message: af_web_dp))
        }
        
        if let awc = installData["awc"] as? String{
            print(helper(module: "awc", message: awc))
        }
    }
}

